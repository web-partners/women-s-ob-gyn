var gulp = require('gulp'),
sass = require('gulp-sass'),
minifycss = require('gulp-minify-css'),
rename = require('gulp-rename'),
uglify = require('gulp-uglify'),
livereload = require('gulp-livereload');

gulp.task('styles', function() {
	return gulp.src('stylesheets/styles.scss')
	.pipe(sass({ style: 'expanded' }))
	.pipe(gulp.dest('css'))
	.pipe(rename({suffix: '.min'}))
	.pipe(minifycss())
	.pipe(gulp.dest('dist/css'));
});

gulp.task('scripts', function () {
	return gulp.src('javascripts/scripts.js')
	.pipe(uglify({outSourceMap: false}))
	.pipe(rename({suffix: '.min'}))
	.pipe(gulp.dest('dist/js'));
});

gulp.task('watch', function() {
  gulp.watch('stylesheets/styles.scss', ['styles']);
  var server = livereload();
  gulp.watch('dist/**').on('change', function(file) {
      server.changed(file.path);
  });
  gulp.watch('javascripts/scripts.js', ['scripts']);
});

gulp.task('default', ['styles', 'watch']);